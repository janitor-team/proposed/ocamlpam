OCAMLC=ocamlc
OCAMLOPT=ocamlopt
OCAMLMKLIB=ocamlmklib
OCAMLMKLIB_OPTS=-oc pam_stubs -lpam

VERSION=1.1
DISTNAME=ocamlpam-$(VERSION)

SOURCES=pam.mli pam.ml
BASENAME=pam_stubs
LIBNAME=lib$(BASENAME).a

DESTDIR=$(shell ocamlc -where)

.PHONY: all dist clean install

all: META byte opt

META: META.in
	sed 's/_VERSION_/$(VERSION)/' META.in > META

$(LIBNAME): pam_stubs.c
	$(OCAMLC) -c pam_stubs.c
	$(OCAMLMKLIB) $(OCAMLMKLIB_OPTS) pam_stubs.o

byte: $(LIBNAME) $(SOURCES)
	$(OCAMLC) -c pam.mli
	$(OCAMLC) -c pam.ml
	$(OCAMLMKLIB) $(OCAMLMKLIB_OPTS) -o pam pam.cmo

opt: $(LIBNAME) $(SOURCES)
	$(OCAMLOPT) -c pam.mli
	$(OCAMLOPT) -c pam.ml
	$(OCAMLMKLIB) $(OCAMLMKLIB_OPTS) -o pam pam.cmx

install:
	install -d $(DESTDIR)/pam
	install -m 644 -t $(DESTDIR)/pam META *.mli *.cmi *.cmx *.cma *.cmxa *.a
	install -d $(DESTDIR)/stublibs
	install -m 644 -t $(DESTDIR)/stublibs dll$(BASENAME).so

dist: clean
	mkdir -p $(DISTNAME)
	-cp * $(DISTNAME)
	tar cvzf $(DISTNAME).tar.gz $(DISTNAME)
	zip -r $(DISTNAME).zip $(DISTNAME)
	rm -fr $(DISTNAME)

clean:
	-rm -fr *.cm* *.a *.o *.so *~ $(DISTNAME).tar.gz $(DISTNAME).zip $(DISTNAME) META
